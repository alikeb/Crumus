using Geo.Data.Context;
using Geo.Models.BaseModels;
using Geo.Models.CountryApi;
using Geo.Models.ViewModels;
using Microsoft.EntityFrameworkCore;
using Country = Geo.Data.Dto.Country;

namespace Geo.Repository;

public class Repository : IRepository
{
    protected readonly EfcContext _db;

    public Repository(EfcContext db)
    {
        _db = db;
    }

    public async Task<Country?> GetOneCountry(long id)
        => await _db.Countries.FirstOrDefaultAsync(x => x.Id == id);

    public async Task<List<Country>> GetAllCountries()
        => await _db.Countries.OrderBy(x => x.Code).ToListAsync();

    public async Task<List<Country>> GetAllProvinces(long CountryId)
        => await _db.Countries.Include(x => x.Provinces).Where(x => x.Id == CountryId).ToListAsync();

    public async Task<bool> FetchCountries(List<MainDetail> model)
    {
        var dtos = model.Select(x => new Country()
        {
            Name = x.Name,
            Symbol = x.Iso3,
            Flag = x.Flag
        });
        await _db.Countries.AddRangeAsync(dtos);
        return await _db.SaveChangesAsync() > 0;
    }

    public async Task<bool> FetchCountryCodes(List<CodeDetail> model)
    {
        Country obj = new();
        var co = await _db.Countries.ToListAsync();
        foreach (var item in model)
        {
            obj = co.FirstOrDefault(x => x.Name.Equals(item.Name));
            if (obj is not null)
            {
                obj.Code = item.Dial_Code;
                _db.Countries.Update(obj);
                await _db.SaveChangesAsync();
               
            }
        }

        return true;
    }

    public async Task<bool> FetchCountryCurrencies(List<CurrencyDetail> model)
    {
        Country obj = new();
        var co = await _db.Countries.ToListAsync();
        foreach (var item in model)
        {
            obj = co.FirstOrDefault(x => x.Name.Equals(item.Name));
            if (obj is not null)
            {
                obj.Currency = item.Currency;
                _db.Countries.Update(obj);
                await _db.SaveChangesAsync();
            }
        }

        return true;
    }
}