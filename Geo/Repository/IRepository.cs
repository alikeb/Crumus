

using Geo.Data.Dto;
using Geo.Models.BaseModels;
using Geo.Models.CountryApi;

namespace Geo.Repository;

public interface IRepository
{
    Task<Country?> GetOneCountry(long id);
    Task<List<Country>> GetAllCountries();
    Task<List<Country>> GetAllProvinces(long countryId);
    Task<bool> FetchCountries(List<MainDetail> model);
    Task<bool> FetchCountryCurrencies(List<CurrencyDetail> model);
    Task<bool> FetchCountryCodes(List<CodeDetail> model);
}