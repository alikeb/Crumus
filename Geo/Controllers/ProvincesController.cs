using Geo.Models.BaseModels;
using Geo.Models.ViewModels;
using Geo.Service.IServices;
using Microsoft.AspNetCore.Mvc;

namespace Geo.Controllers;

[Produces("application/json")]
[Route("V1/[controller]")]
public class ProvincesController
{
    private readonly IProvinceServices _ProvinceServices;

    public ProvincesController(IProvinceServices provinceServices)
    {
        _ProvinceServices = provinceServices;
    }

    [HttpGet(nameof(GetAll))]
    public async Task<IActionResult> GetAll(long CountryId)
        => new Respond<IEnumerable<Country>>().ActionRespond(await _ProvinceServices.GetAll(CountryId));
}