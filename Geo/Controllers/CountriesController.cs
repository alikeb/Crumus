using Geo.Models.BaseModels;
using Geo.Models.ViewModels;
using Geo.Service.IServices;
using Microsoft.AspNetCore.Mvc;

namespace Geo.Controllers;

[Produces("application/json")]
[Route("V1/[controller]")]
public class CountriesController:Controller
{
    private readonly ICountryServices _countryServices;
    private readonly IGeoServices _geoServices;

    public CountriesController(ICountryServices countryServices,IGeoServices geoServices)
    {
        _countryServices = countryServices;
        _geoServices = geoServices;
    }
    [HttpGet(nameof(FetchCodes))]
    public async Task<IActionResult> FetchCodes()
        => new Respond<bool>().ActionRespond(await _geoServices.FetchCountryCodes());
    [HttpGet(nameof(FetchCountries))]
    public async Task<IActionResult> FetchCountries()
        => new Respond<bool>().ActionRespond(await _geoServices.FetchCountries());
    [HttpGet(nameof(FetchCountryCurrencies))]
    public async Task<IActionResult> FetchCountryCurrencies()
        => new Respond<bool>().ActionRespond(await _geoServices.FetchCountryCurrencies());
    
    [HttpGet(nameof(GetOne))]
    public async Task<IActionResult> GetOne(long id)
        => new Respond<Country>().ActionRespond(await _countryServices.GetOne(id));
    
    [HttpGet(nameof(GetAll))]
    public async Task<IActionResult> GetAll()
        => new Respond<IEnumerable<Country>>().ActionRespond(await _countryServices.GetAll());
}