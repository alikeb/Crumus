// using Geo.Data.Context;
using Geo.Repository;
using Geo.Service.ExternalServices;
using Geo.Service.IExternalServices;
using Geo.Service.IServices;
using Geo.Service.Services;


namespace Geo.Utils;


public static class InjectHandler
{
    public static IServiceCollection InjectServices(this IServiceCollection services)
    {
        services.AddScoped<ICountryServices, CountryServices>();
        services.AddScoped<IProvinceServices, ProvinceServices>();
        services.AddScoped<IGeoServices, GeoServices>();
        services.AddScoped<IExternalGeoService, ExternalGeoService>();
        services.AddScoped<IRepository, Repository.Repository>();
        services.AddScoped<BaseService>();
        // services.AddScoped<Context>();
        return services;
    }
}