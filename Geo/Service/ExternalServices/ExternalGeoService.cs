using Geo.Models.BaseModels;
using Geo.Models.CountryApi;
using Geo.Service.IExternalServices;

namespace Geo.Service.ExternalServices;

public class ExternalGeoService : BaseService,IExternalGeoService
{
    public async Task<CountryApiBaseModel<MainDetail>?> FetchCountries()
    {
        var res = await HttpGetRequest<CountryApiBaseModel<MainDetail>>("countries/flag/images", 1);
        if (res.Error)
        {
            return null;
        }

        return res;
    }
    public async Task<CountryApiBaseModel<CodeDetail?>> FetchCountryCodes()
    {
        var res = await HttpGetRequest<CountryApiBaseModel<CodeDetail>>("countries/codes", 1);
        if (res.Error)
        {
            return null;
        }

        return res;
    }
    public async Task<CountryApiBaseModel<CurrencyDetail?>> FetchCountryCurrencies()
    {
        var res = await HttpGetRequest<CountryApiBaseModel<CurrencyDetail>>("countries/currency", 1);
        if (res.Error)
        {
            return null;
        }

        return res;
    }
}