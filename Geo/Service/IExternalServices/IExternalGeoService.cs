using Geo.Models.BaseModels;
using Geo.Models.CountryApi;
using Geo.Models.ViewModels;

namespace Geo.Service.IExternalServices;

public interface IExternalGeoService
{
     Task<CountryApiBaseModel<MainDetail?>> FetchCountries();
     Task<CountryApiBaseModel<CodeDetail?>> FetchCountryCodes();
     Task<CountryApiBaseModel<CurrencyDetail?>> FetchCountryCurrencies();
}