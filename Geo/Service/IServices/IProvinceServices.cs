using Geo.Models.BaseModels;
using Geo.Models.ViewModels;

namespace Geo.Service.IServices;

public interface IProvinceServices
{
    Task<ResultModel<IEnumerable<Country>>> GetAll(long countryId);
}