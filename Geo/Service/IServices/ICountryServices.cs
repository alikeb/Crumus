using Geo.Models.BaseModels;
using Geo.Models.ViewModels;

namespace Geo.Service.IServices;

public interface ICountryServices
{
     Task<ResultModel<Country>> GetOne(long id);

     Task<ResultModel<IEnumerable<Country>>>GetAll();
}