using Geo.Models.BaseModels;

namespace Geo.Service.IServices;

public interface IGeoServices
{
    Task<ResultModel<bool>> FetchCountryCodes();
    Task<ResultModel<bool>> FetchCountries();
    Task<ResultModel<bool>> FetchCountryCurrencies();
}