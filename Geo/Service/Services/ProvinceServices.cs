using Geo.Models.BaseModels;
using Geo.Models.ViewModels;
using Geo.Repository;
using Geo.Service.IServices;

namespace Geo.Service.Services;

public class ProvinceServices:IProvinceServices
{
    private readonly IRepository _repository;

    public ProvinceServices(IRepository repository)
    {
        _repository = repository;
    }

    public  async Task<ResultModel<IEnumerable<Country>>> GetAll(long countryId)
    {
       var objList = await  _repository.GetAllProvinces(countryId);
       var res = objList.Select(x => new Country()
       {
           Code = x.Code,
           Flag = x.Flag,
           Id = x.Id,
           Name = x.Name,
           Symbol = x.Symbol,
           Provinces = x.Provinces.Select(x => new Province()
           {
               Id = x.Id,
               Name = x.Name
           }).ToList()
       });
       return new ResultModel<IEnumerable<Country>>(res);
    }
}