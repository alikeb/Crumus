using Geo.Models.BaseModels;
using Geo.Models.ViewModels;
using Geo.Repository;
using Geo.Service.IServices;

namespace Geo.Service.Services;

public class CountryServices:ICountryServices
{
    private readonly IRepository _repository;

    public CountryServices(IRepository repository)
    {
        _repository = repository;
    }

    public async Task<ResultModel<Country>> GetOne(long id)
    {
        var obj = await _repository.GetOneCountry(id);
         var res = new Country()
         {
             Code = obj.Code,
             Flag = obj.Flag,
             Id = obj.Id,
             Name = obj.Name
         };
         return new ResultModel<Country>(res);
    }

    public async Task<ResultModel<IEnumerable<Country>>> GetAll()
    {
        var objList = await _repository.GetAllCountries();
        var res = objList.Select(x => new Country()
        {
            Code = x.Code,
            Flag = x.Flag,
            Id = x.Id,
            Name = x.Name,
            Symbol = x.Symbol,
            Currency = x.Currency
        });
        return new ResultModel<IEnumerable<Country>>(res);
    }
}