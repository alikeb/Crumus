using Geo.Models.BaseModels;
using Geo.Repository;
using Geo.Service.IExternalServices;
using Geo.Service.IServices;

namespace Geo.Service.Services;

public class GeoServices:IGeoServices
{
    private readonly IExternalGeoService _externalGeoService;
    private readonly IRepository _repository;

    public GeoServices(IExternalGeoService externalGeoService, IRepository repository)
    {
        _externalGeoService = externalGeoService;
        _repository = repository;
    }

    public async Task<ResultModel<bool>> FetchCountries()
    {
        var res = await _externalGeoService.FetchCountries();
        if (res != null)
        {
            var dbRes = await _repository.FetchCountries(res.Data);
            return new ResultModel<bool>(dbRes);
        }

        return new ResultModel<bool>(false);
       
    }
    public async Task<ResultModel<bool>> FetchCountryCodes()
    {
        var res = await _externalGeoService.FetchCountryCodes();
        if (res != null)
        {
            var dbRes = await _repository.FetchCountryCodes(res.Data);
            return new ResultModel<bool>(dbRes);
        }

        return new ResultModel<bool>(false);
       
    }
    public async Task<ResultModel<bool>> FetchCountryCurrencies()
    {
        var res = await _externalGeoService.FetchCountryCurrencies();
        if (res != null)
        {
            var dbRes = await _repository.FetchCountryCurrencies(res.Data);
            return new ResultModel<bool>(dbRes);
        }

        return new ResultModel<bool>(false);
       
    }
}