namespace Geo.Models.BaseModels;

public class CountryApiBaseModel<T>
{
    public bool Error { get; set; }
    public string Msg { get; set; }
    public List<T> Data { get; set; }
}