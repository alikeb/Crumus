namespace Geo.Models.CountryApi;

public class CodeDetail
{
    public string Name { get; set; }
    public string Dial_Code { get; set; }
}