namespace Geo.Models.CountryApi;

public class CurrencyDetail
{
    public string Name { get; set; }
    public string Currency { get; set; }
}