namespace Geo.Models.CountryApi;

public class MainDetail
{
    public string Name { get; set; }
    public string Flag { get; set; }
    public string Iso2 { get; set; }
    public string Iso3 { get; set; }
}