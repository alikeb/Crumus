namespace Geo.Models.ViewModels;

public class City
{
    public long Id { get; set; }
    public string Name { get; set; }
    public Province Province { get; set; }
}