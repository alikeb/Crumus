namespace Geo.Models.ViewModels;

public class Province
{
    public long Id { get; set; }
    public Country Country { get; set; }
    public string Name { get; set; }
}