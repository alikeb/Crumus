namespace Geo.Models.ViewModels;

public class Country
{
    public long Id { get; set; }
    public string? Code { get; set; }
    public string Symbol { get; set; }
    public string Name { get; set; }
    public string? Flag { get; set; }
    public string Currency { get; set; }
    public List<Province> Provinces { get; set; }
}