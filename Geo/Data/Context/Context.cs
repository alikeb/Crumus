using Geo.Data.Dto;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Geo.Data.Context;

public class EfcContext: DbContext
{
    private readonly IConfiguration _configuration;
    private readonly string _connectionString;

    public EfcContext(DbContextOptions<EfcContext> options)
        : base(options)
    {
    }
    public virtual DbSet<Country> Countries { get; set; } 
    public virtual DbSet<Province> Provinces { get; set; } 
    public virtual DbSet<City> Cities { get; set; } 

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var connectionString = configuration.GetConnectionString("SqlConnection");
            optionsBuilder.UseSqlServer(connectionString);
        }
    }
    // protected override void OnModelCreating(ModelBuilder modelBuilder)
    // {
    //     base.OnModelCreating(modelBuilder);
    // }
   
    
}
public class YourDbContextFactory : IDesignTimeDbContextFactory<EfcContext>
{
    public EfcContext CreateDbContext(string[] args)
    {
        var optionsBuilder = new DbContextOptionsBuilder<EfcContext>();
        var configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json")
            .Build();
        var connectionString = configuration.GetConnectionString("SqlConnection");
        optionsBuilder.UseSqlServer(connectionString);

        return new EfcContext(optionsBuilder.Options);
    }
}