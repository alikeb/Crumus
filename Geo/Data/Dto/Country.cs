using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Geo.Data.Dto;

public class Country
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public long Id { get; set; }
    
    public string Name { get; set; }
    public string? Flag { get; set; }
    public string? Code { get; set; }
    public string Symbol { get; set; }
    public string? Currency { get; set; }
    public ICollection<Province> Provinces { get; set; }
}