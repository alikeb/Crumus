using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Geo.Data.Dto;

public class Province
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public long Id { get; set; }

    [ForeignKey("Countries")]
    public long CountryId { get; set; }
    public string Name { get; set; }
    public ICollection<City> Cities { get; set; }
}