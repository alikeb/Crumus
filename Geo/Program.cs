// using Geo.Data.Context;

using Geo.Data.Context;
using Geo.Utils;
using Microsoft.EntityFrameworkCore;
using Serilog;

try
{
    var webApplicationOptions = new WebApplicationOptions()
    {
        ContentRootPath = AppContext.BaseDirectory, Args = args,
        ApplicationName = System.Diagnostics.Process.GetCurrentProcess().ProcessName
    };
    var builder = WebApplication.CreateBuilder(webApplicationOptions);

    #region Serilog
    var configuration = new ConfigurationBuilder()
        .AddJsonFile("appsettings.json", false, true)
        .Build();
    Log.Logger = new LoggerConfiguration()
        .ReadFrom.Configuration(configuration)
        //.WriteTo.File(AppDomain.CurrentDomain.BaseDirectory + "logs\\apilog.txt")
        //.MinimumLevel.wa()
        .CreateLogger();

    builder.Logging.ClearProviders();
    builder.Host.UseSerilog();
    Log.Debug("Starting up");

    #endregion
// Add services to the container.

    builder.Services.AddControllers();
    builder.Services.InjectServices();
   builder.Host.UseWindowsService();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen();

    builder.Services.AddDbContext<EfcContext>(options =>
    {
        options.UseSqlServer(builder.Configuration.GetConnectionString("SqlConnection"),
            sqlServerOptionsAction: sqlOptions =>
            {
                sqlOptions.EnableRetryOnFailure(
                    maxRetryCount: 10,
                    maxRetryDelay: TimeSpan.FromSeconds(30),
                    errorNumbersToAdd: null);
            });
    });
    
    var app = builder.Build();

// Configure the HTTP request pipeline.
    // if (app.Environment.IsDevelopment())
    // {
        app.UseSwagger();
        app.UseSwaggerUI();
    // }

    app.UseHttpsRedirection();

    app.UseAuthorization();

    app.MapControllers();

    app.Run();
}
catch (Exception e)
{
    Log.Debug(e.Message);
    if (e.InnerException != null)
        Log.Debug(e.InnerException.Message);
}
finally
{
    Log.Debug("Shut down complete");
    //  Log.CloseAndFlush();
}