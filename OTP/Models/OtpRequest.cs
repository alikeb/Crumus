using System.ComponentModel.DataAnnotations;

namespace OTP.Models;

public class OtpRequest
{
    [Required]
    public string Email { get; set; }
}