using System.ComponentModel.DataAnnotations;

namespace OTP.Models;

public class CheckRequest
{
    [Required]
    public string Email { get; set; }
    [Required]
    public string OtpId { get; set; }
    [Required]
    public string Code { get; set; }
}