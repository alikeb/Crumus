using OTP.Utils;
using Serilog;

try
{
    var webApplicationOptions = new WebApplicationOptions()
    {
        ContentRootPath = AppContext.BaseDirectory, Args = args,
        ApplicationName = System.Diagnostics.Process.GetCurrentProcess().ProcessName
    };
    var builder = WebApplication.CreateBuilder(webApplicationOptions);

    #region Serilog
    var configuration = new ConfigurationBuilder()
        .AddJsonFile("appsettings.json", false, true)
        .Build();
    Log.Logger = new LoggerConfiguration()
        .ReadFrom.Configuration(configuration)
        //.WriteTo.File(AppDomain.CurrentDomain.BaseDirectory + "logs\\apilog.txt")
        //.MinimumLevel.wa()
        .CreateLogger();

    builder.Logging.ClearProviders();
    builder.Host.UseSerilog();
    Log.Debug("Starting up");

    #endregion
    builder.Services.AddControllers();
    builder.Services.InjectServices();
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen();
    builder.Host.UseWindowsService();
    var app = builder.Build();


    app.UseSwagger();
    app.UseSwaggerUI();


    app.UseHttpsRedirection();

    app.UseAuthorization();

    app.MapControllers();

    app.Run();
}
catch (Exception e)
{
    Log.Debug(e.Message);
    if (e.InnerException != null)
        Log.Debug(e.InnerException.Message);
}
finally
{
    Log.Debug("Shut down complete");
  //  Log.CloseAndFlush();
}