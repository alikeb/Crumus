using OnBoarding.Models.Input;

namespace OnBoarding.Repository;

public interface IOnBoardingRepository
{
    Task<long> StepOne(StepOneForm model);
    Task<bool> StepTwo(StepTwoForm model);
    Task<bool> StepThree(StepThreeForm model);
    Task<bool> StepFour(StepFourForm model);
}