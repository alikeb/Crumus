using Microsoft.EntityFrameworkCore;
using OnBoarding.Data.Context;
using OnBoarding.Data.Dto;
using OnBoarding.Models.Input;

namespace OnBoarding.Repository;

public class OnBoardingRepository : IOnBoardingRepository
{
    protected readonly EfcContext _db;

    public OnBoardingRepository(EfcContext db)
    {
        _db = db;
    }

    public async Task<long> StepOne(StepOneForm model)
    {
        Progress obj = new()
        {
            Email = model.Email,
            FirstName = model.FirstName,
            LastName = model.LastName,
            Gender = model.Gender,
            BirthDate = model.BirthDate,
            Step = 1
        };
        var res = await _db.Progresses.AddAsync(obj);
        if (await _db.SaveChangesAsync() > 0)
        {
            return obj.Id;
        }

        return 0;
    }

    public async Task<bool> StepTwo(StepTwoForm model)
    {
        var res = await _db.Progresses.FirstOrDefaultAsync(x => x.Id == model.Id);
        res.Nationalcode = model.NationalCode;
        res.PassportNumber = model.PassportNumber;
        res.NationalCard = model.NationalCard;
        res.Passport = model.Passport;
        res.Step = 2;
        _db.Progresses.Update(res);
        return await _db.SaveChangesAsync() > 0;
    }

    public async Task<bool> StepThree(StepThreeForm model)
    {
        var res = await _db.Progresses.FirstOrDefaultAsync(x => x.Id == model.Id);
        res.Address = model.Address;
        res.CityId = model.CityId;
        res.ZipCode = model.ZipCode;
        res.Step = 3;
        _db.Progresses.Update(res);
        return await _db.SaveChangesAsync() > 0;
    }

    public async Task<bool> StepFour(StepFourForm model)
    {
        var res = await _db.Progresses.FirstOrDefaultAsync(x => x.Id == model.Id);
        res.UserName = model.UserName;
        res.Password = model.Password;
        res.Step = 4;
        _db.Progresses.Update(res);
        return await _db.SaveChangesAsync() > 0;
    }
}