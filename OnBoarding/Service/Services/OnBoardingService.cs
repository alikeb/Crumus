using Microsoft.Net.Http.Headers;
using OnBoarding.Models.BaseModels;
using OnBoarding.Models.Input;
using OnBoarding.Repository;
using OnBoarding.Service.IServices;

namespace OnBoarding.Service.Services;

public class OnBoardingService:IOnBoardingService
{
    private readonly IOnBoardingRepository _repository;

    public OnBoardingService(IOnBoardingRepository repository)
    {
        _repository = repository;
    }

    public async Task<ResultModel<long>> StepOne(StepOneForm model)
        => new (await _repository.StepOne(model));

   

    public async Task<ResultModel<bool>> StepTwo(StepTwoFile model)
    {
        var passport = await ImageSaver(model.Passport, model.PassportNumber, "Passport");
        var nationalCard = await ImageSaver(model.NationalCard, model.NationalCode, "NationalCard");
        if (string.IsNullOrEmpty(passport) || string.IsNullOrEmpty(nationalCard))
        {
            return new (false);
        }

        return new(await _repository.StepTwo(new()
        {
            Id = model.Id, NationalCode = model.NationalCode, PassportNumber = model.PassportNumber,
            Passport = passport, NationalCard = nationalCard
        }));
    }

    public async Task<ResultModel<bool>> StepThree(StepThreeForm model)
        => new(await _repository.StepThree(model));

    public async Task<ResultModel<bool>> StepFour(StepFourForm model)
    {
        return new ResultModel<bool>(await _repository.StepFour(model));
    }

    public async Task<string> ImageSaver(IFormFile model, string passportNumber, string docType)
    {
        try
        {
            var file = model;
            var storePath = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build()
                .GetSection("storePath").Value;
            var folderName = Path.Combine("DocumentArchive", docType);
            var pathToSave = Path.Combine(storePath, folderName);
            if (file.Length > 0)
            {
                var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim();
                var a = fileName.Split(new[] { '.' });
                var extension = a.LastOrDefault().ToString();
                var fullPath = Path.Combine(pathToSave, passportNumber +"."+extension);

                if (!Directory.Exists(pathToSave))
                {
                    Directory.CreateDirectory(pathToSave);
                }

                using (var stream =
                       new FileStream(fullPath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite))
                {
                    await file.CopyToAsync(stream);
                }

                return fullPath;
            }
        }
        catch (Exception ex)
        {
            return null;
        }

        return null;
    }
}