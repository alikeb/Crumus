using OnBoarding.Models.BaseModels;
using OnBoarding.Models.Input;

namespace OnBoarding.Service.IServices;

public interface IOnBoardingService
{
    Task<ResultModel<long>> StepOne(StepOneForm model);
    Task<ResultModel<bool>> StepTwo(StepTwoFile model);
    Task<ResultModel<bool>> StepThree(StepThreeForm model);
    Task<ResultModel<bool>> StepFour(StepFourForm model);
}