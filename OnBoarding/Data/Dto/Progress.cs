using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OnBoarding.Data.Dto;

public class Progress
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public long Id { get; set; }
    public string? Email { get; set; }
    public string? UserName { get; set; }
    public string? Password { get; set; }
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
    public int? Gender { get; set; }
    public DateTime? BirthDate { get; set; }
    public string? Nationalcode { get; set; }
    public string? PassportNumber { get; set; }
    public string? NationalCard { get; set; }
    public string? Passport { get; set; }
    public string? Address { get; set; }
    public string? ZipCode { get; set; }
    public long? CityId { get; set; }
    public DateTime CreatedAt { get; set; }
    public int Step { get; set; }
    public bool? IsRemoved { get; set; }
}