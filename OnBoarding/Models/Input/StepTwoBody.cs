namespace OnBoarding.Models.Input;

public class StepTwoBody
{
    public long Id { get; set; }
    public string NationalCode { get; set; }
    public string PassportNumber { get; set; }
}