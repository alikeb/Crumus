namespace OnBoarding.Models.Input;

public class StepOneForm
{
    public string Email { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public int Gender { get; set; }
    public DateTime BirthDate { get; set; }
}