namespace OnBoarding.Models.Input;

public class StepThreeForm
{
    public long Id { get; set; }
    public string Address { get; set; }
    public string ZipCode { get; set; }
    public long CityId { get; set; }
    
}