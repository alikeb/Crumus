namespace OnBoarding.Models.Input;

public class StepFourForm
{
    public long Id { get; set; }
    public string UserName { get; set; }
    public string Password { get; set; }
}