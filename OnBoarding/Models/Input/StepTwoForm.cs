namespace OnBoarding.Models.Input;

public class StepTwoForm
{
    public long Id { get; set; }
    public string NationalCode { get; set; }
    public string PassportNumber { get; set; }
    public string NationalCard { get; set; }
    public string Passport { get; set; }
}