namespace OnBoarding.Models.Input;

public class StepTwoFile
{
   
    public IFormFile NationalCard { get; set; }
    public IFormFile Passport { get; set; }
    public long Id { get; set; }
    public string NationalCode { get; set; }
    public string PassportNumber { get; set; }
}