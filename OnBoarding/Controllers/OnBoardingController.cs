using Microsoft.AspNetCore.Mvc;
using OnBoarding.Models.BaseModels;
using OnBoarding.Models.Input;
using OnBoarding.Service.IServices;

namespace OnBoarding.Controllers;

[Produces("application/json")]
[Route("V1/[controller]")]
public class OnBoardingController : Controller
{
    private readonly IOnBoardingService _onBoardingService;

    public OnBoardingController(IOnBoardingService onBoardingService)
    {
        _onBoardingService = onBoardingService;
    }

    [HttpPost(nameof(StepOne))]
    public async Task<IActionResult> StepOne([FromBody]StepOneForm model)
        => new Respond<long>().ActionRespond(await _onBoardingService.StepOne(model));

    [HttpPost(nameof(StepTwo))]
    public async Task<IActionResult> StepTwo(StepTwoFile model)
        => new Respond<bool>().ActionRespond(await _onBoardingService.StepTwo(model));

    
    [HttpPost(nameof(StepThree))]
    public async Task<IActionResult> StepThree([FromBody]StepThreeForm model)
        => new Respond<bool>().ActionRespond(await _onBoardingService.StepThree(model));
    
    [HttpPost(nameof(StepFour))]
    public async Task<IActionResult> StepFour([FromBody]StepFourForm model)
        => new Respond<bool>().ActionRespond(await _onBoardingService.StepFour(model));
}