using OnBoarding.Repository;
using OnBoarding.Service.IServices;
using OnBoarding.Service.Services;

namespace OnBoarding.Utils;

public static class InjectHandler
{
    public static IServiceCollection InjectServices(this IServiceCollection services)
    {
        services.AddScoped<IOnBoardingService, OnBoardingService>();
        services.AddScoped<IOnBoardingRepository, OnBoardingRepository>();
        

        return services;
    }
}