using AAA.Models.BaseModels;
using AAA.Models.Requests;
using AAA.Models.ViewModels;
using User = AAA.Data.Dto.User;

namespace AAA.Repository;

public interface IUserRepository
{
    Task<bool>CreateOne(AddOne model);
    Task<User?> GetOne(string sessionId);
    Task<User?>GetOneByEmail(string email);
    Task<bool> CheckSession(long userId);
}