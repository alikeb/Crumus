using AAA.Data.Context;
using AAA.Models.BaseModels;
using AAA.Models.Requests;
using AAA.Models.ViewModels;
using AAA.Service.IServices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using User = AAA.Data.Dto.User;

namespace AAA.Repository;

public class UsersRepository : IUserRepository
{
    protected readonly EfcContext _db;

    public UsersRepository(EfcContext db)
    {
        _db = db;
    }

    public async Task<bool> CreateOne(AddOne model)
    {
        try
        {
            await _db.Users.AddAsync(new()
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Gender = model.Gender,
                BirthDate = model.Birth,
                Email = model.Email
            });

            return await _db.SaveChangesAsync() > 0;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public async Task<User?> GetOne(string sessionId)
        => await _db.Users.Where(x => x.SessionId.Equals(sessionId)).FirstOrDefaultAsync();

    public async Task<User?> GetOneByEmail(string email)
        => await _db.Users.Where(x => x.Email.Equals(email)).FirstOrDefaultAsync();

    public async Task<bool> CheckSession(long userId)
        => await _db.Users.AnyAsync(x => x.Id == userId && x.SessionId != null);
}