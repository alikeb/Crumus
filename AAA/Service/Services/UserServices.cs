using AAA.Models.BaseModels;
using AAA.Models.Requests;
using AAA.Models.ViewModels;
using AAA.Repository;
using AAA.Service.IServices;

namespace AAA.Service.Services;

public class UserServices : IUserServices
{
    private readonly IUserRepository _userRepository;

    public UserServices(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }


    public async Task<ResultModel<bool>> CreateOne(AddOne model)
        => new(await _userRepository.CreateOne(model));

    public async Task<ResultModel<User>> GetOne(string sessionId)
    {
        var obj = await _userRepository.GetOne(sessionId);
        var res = new User()
        {
            Email = obj.Email,
            MobileNumber = obj.MobileNumber,
            UserName = obj.UserName,
            // NationalCode = obj.NationalCode,
        };
        return new ResultModel<User>(res);
    }


    public async Task<ResultModel<bool>> CheckSession(long userId)
        =>new (await _userRepository.CheckSession(userId));

    public async Task<ResultModel<User>> GetOneByEmail(string email)
    {
        var obj = await _userRepository.GetOneByEmail(email);
        var res = new User()
        {
            Email = obj.Email,
            MobileNumber = obj.MobileNumber,
            UserName = obj.UserName,
            // NationalCode = obj.NationalCode,
        };
        return new ResultModel<User>(res);
    }
}