using AAA.Models.BaseModels;
using AAA.Models.Requests;
using AAA.Models.ViewModels;

namespace AAA.Service.IServices;

public interface IUserServices
{
    Task<ResultModel<bool>> CreateOne(AddOne model);
    Task<ResultModel<User>> GetOne(string sessionId);
    Task<ResultModel<User>> GetOneByEmail(string email);
    Task<ResultModel<bool>> CheckSession(long userId);
}