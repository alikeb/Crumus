namespace AAA.Data.Dto;

public class Customer
{
public long Id { get; set; }
public long UserId { get; set; }
public string? NationalCode { get; set; }
public string? PassportNumber { get; set; }
public string? NationalCard { get; set; }
public string? PassPort { get; set; }
public long? CityId { get; set; }
public string? ZipCode { get; set; }
public string? Address { get; set; }
public DateTime? CreatedAt { get; set; }
public bool? IsRemoved { get; set; }
}