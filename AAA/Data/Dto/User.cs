using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AAA.Models;
using Microsoft.EntityFrameworkCore;

namespace AAA.Data.Dto;

[Index(nameof(UserName), nameof(MobileNumber), nameof(Email), nameof(SessionId), IsUnique = true)]
public class User
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public long Id { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }
    [MaxLength(20)] public string? UserName { get; set; }

    public string? Password { get; set; }
    [Phone] public string? MobileNumber { get; set; }
  
    [EmailAddress] public string? Email { get; set; }

    public Gender Gender { get; set; }
    
    //public string? NationalCode { get; set; }
   
    public bool? IsRemoved { get; set; }
    
   
    public bool? IsActive { get; set; }
    
    public DateTime BirthDate { get; set; }

   
    public DateTime? CreatedAt { get; set; }

    public Guid? SessionId { get; set; }
    public DateTime? SessionAt { get; set; }
   public virtual Customer? Customers { get; set; }
}