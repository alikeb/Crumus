using AAA.Data.Dto;
using Microsoft.EntityFrameworkCore;

namespace AAA.Data.Context;

public class EfcContext: DbContext
{
    private readonly IConfiguration _configuration;
    private readonly string _connectionString;

    public EfcContext(DbContextOptions<EfcContext> options)
        : base(options)
    {
    }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var connectionString = configuration.GetConnectionString("SqlConnection");
            optionsBuilder.UseSqlServer(connectionString);
        }
    }
    public new async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new())
    {
        var a = ChangeTracker.Entries().Where(e => e.State == EntityState.Added /*|| e.State == EntityState.Modified*/);
        foreach (var entry in a)
        {
            if (entry.State == EntityState.Added)
            {
                if (entry.Entity.GetType().GetProperty("CreatedAt") != null)
                {
                    entry.Entity.GetType().GetProperty("CreatedAt").SetValue(entry.Entity, DateTime.Now);
                }
                if (entry.Entity.GetType().GetProperty("IsActive") != null)
                {
                    entry.Entity.GetType().GetProperty("IsActive").SetValue(entry.Entity, false);
                }
                if (entry.Entity.GetType().GetProperty("IsRemoved") != null)
                {
                    entry.Entity.GetType().GetProperty("IsRemoved").SetValue(entry.Entity, false);
                }
            }
            // if (entry.State == EntityState.Modified)
            // {
            //     if (entry.Entity.GetType().GetProperty("UpdatedAt") != null)
            //     {
            //         entry.Entity.GetType().GetProperty("UpdatedAt").SetValue(entry.Entity, DateTime.Now);
            //     }
            // }
        }

        return await base.SaveChangesAsync(cancellationToken);
    }
    public virtual DbSet<User?> Users { get; set; } = null!;
}