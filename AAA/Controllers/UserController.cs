using AAA.Models.BaseModels;
using AAA.Models.Requests;
using AAA.Models.ViewModels;
using AAA.Service.IServices;
using Microsoft.AspNetCore.Mvc;

namespace AAA.Controllers;

[Produces("application/json")]
[Route("V1/[controller]")]
public class UserController : Controller
{
    private readonly IUserServices _userServices;

    public UserController(IUserServices userServices)
    {
        _userServices = userServices;
    }

    [HttpPost(nameof(CreateOne))]
    public async Task<IActionResult> CreateOne([FromBody]AddOne model)
        => new Respond<bool>().ActionRespond(await _userServices.CreateOne(model));
    
    [HttpGet(nameof(GetOne))]
    public async Task<IActionResult> GetOne(string sessionId)
        => new Respond<User>().ActionRespond(await _userServices.GetOne(sessionId));
    
    [HttpGet(nameof(GetOneByEmail))]
    public async Task<IActionResult> GetOneByEmail(string Email)
        => new Respond<User>().ActionRespond(await _userServices.GetOneByEmail(Email));
    
    [HttpGet(nameof(CheckSession))]
    public async Task<IActionResult> CheckSession(long userId)
        => new Respond<bool>().ActionRespond(await _userServices.CheckSession(userId));
    
  
}