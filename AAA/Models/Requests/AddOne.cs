using System.ComponentModel.DataAnnotations;

namespace AAA.Models.Requests;

public class AddOne
{
    [Required]
    public string FirstName { get; set; }
    [Required]
    public string LastName { get; set; }
    [Required]
    public Gender Gender { get; set; }
    [Required]
    public DateTime Birth { get; set; }
    [Required]
    public string Email { get; set; }
}