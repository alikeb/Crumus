namespace AAA.Models.ViewModels;

public class User
{
    public string? UserName { get; set; }
    public string? MobileNumber { get; set; }
    // public string? NationalCode { get; set; }
    public string? Email { get; set; }
   // public bool IsActive { get; set; }
   // public DateTime CreatedAt { get; set; }
   // public Guid? SessionId { get; set; }
   // public DateTime? SessionAt { get; set; }
}