using AAA.Repository;
using AAA.Service.IServices;
using AAA.Service.Services;

namespace AAA.Utils;

public static class InjectHandler
{
    public static IServiceCollection InjectServices(this IServiceCollection services)
    {
        services.AddScoped<IUserServices, UserServices>();
        services.AddScoped<IUserRepository, UsersRepository>();
        
       
        return services;
    }
}