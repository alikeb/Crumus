using System.Reflection.Metadata;
using CM.Models.BaseModels;
using CM.Models.Requests;
using CM.Service.IServices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;

namespace CM.Controllers;

[Produces("application/json")]
[Route("V1/[controller]")]
public class CustomersController : Controller
{
    private readonly ICustomerServices _customerServices;

    public CustomersController(ICustomerServices customerServices)
    {
        _customerServices = customerServices;
    }

    [HttpPost(nameof(CreateOne))]
    public async Task<IActionResult> CreateOne([FromBody] CreateOne model)
        => new Respond<bool>().ActionRespond(await _customerServices.CreateOne(model));

    [HttpPost(nameof(CustomerDocs))]
    public async Task<IActionResult> CustomerDocs([FromForm] DocumentForm model)
        => new Respond<bool>().ActionRespond(await _customerServices.CustomerDocuments(model));
    
    [HttpPost(nameof(SetAddress))]
    public async Task<IActionResult> SetAddress([FromBody] AdressForm model)
        => new Respond<bool>().ActionRespond(await _customerServices.SetAddress(model));
}