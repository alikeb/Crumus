using CM.Repository;
using CM.Service.IServices;
using CM.Service.Services;


namespace CM.Utils;

public static class InjectHandler
{
    public static IServiceCollection InjectServices(this IServiceCollection services)
    {
        services.AddScoped<ICustomerServices, CustomerServices>();
        services.AddScoped<ICustomerRepository, CustomerRepository>();
        return services;
    }
}