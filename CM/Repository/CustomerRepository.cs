using CM.Data.Context;
using CM.Data.Dto;
using CM.Models.BaseModels;
using CM.Models.Requests;
using Microsoft.EntityFrameworkCore;

namespace CM.Repository;

public class CustomerRepository : ICustomerRepository
{
    protected readonly EfcContext _db;

    public CustomerRepository(EfcContext db)
    {
        _db = db;
    }

    public async Task<bool> CreateOne(CreateOne model)
    {
        await _db.AddAsync(new Customer()
        {
            UserId = model.UserId
        });
        return await _db.SaveChangesAsync() > 0;
    }
    public async Task<bool> CustomerDocuments(DocumentForm model, string passportPath, string nationalCardPath)
    {
        var c = await _db.Customers.FirstOrDefaultAsync(x => x.UserId == model.UserId);
        c.NationalCard = nationalCardPath;
        c.PassPort = passportPath;
        c.NationalCode = model.NationalCode;
        c.PassportNumber = model.PassportNumber;
         _db.Update(c);
      
        return await _db.SaveChangesAsync() > 0;
    }

    public async Task<bool> SetAdress(AdressForm model)
    {
        var c = await _db.Customers.FirstOrDefaultAsync(x => x.UserId == model.UserId);
        c.Address = model.Address;
        c.ZipCode = model.Zipcode;
        c.CityId = model.CityId;
        return await _db.SaveChangesAsync() > 0;
    }
}