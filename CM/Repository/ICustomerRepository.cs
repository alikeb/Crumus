using CM.Models.BaseModels;
using CM.Models.Requests;

namespace CM.Repository;

public interface ICustomerRepository
{
    Task<bool>CreateOne(CreateOne model);
    Task<bool> CustomerDocuments(DocumentForm model, string passportPath, string nationalCardPath);
    Task<bool> SetAdress(AdressForm model);
}