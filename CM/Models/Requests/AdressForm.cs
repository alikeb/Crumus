namespace CM.Models.Requests;

public class AdressForm
{
    public long UserId { get; set; }
    public string Address { get; set; }
    public string Zipcode { get; set; }
    public long CityId { get; set; }
}