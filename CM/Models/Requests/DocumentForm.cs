namespace CM.Models.Requests;

public class DocumentForm
{
    public long UserId { get; set; }
    public string PassportNumber { get; set; }
    public IFormFile Passport { get; set; }
    public string NationalCode { get; set; }
    public IFormFile NationalCard { get; set; }
}