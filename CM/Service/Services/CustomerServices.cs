using CM.Models.BaseModels;
using CM.Models.Requests;
using CM.Repository;
using CM.Service.IServices;
using Microsoft.Net.Http.Headers;

namespace CM.Service.Services;

public class CustomerServices : ICustomerServices
{
    private readonly ICustomerRepository _customerRepository;

    public CustomerServices(ICustomerRepository customerRepository)
    {
        _customerRepository = customerRepository;
    }

    public async Task<ResultModel<bool>> CreateOne(CreateOne model)
        => new(await _customerRepository.CreateOne(model));


    public async Task<ResultModel<bool>> CustomerDocuments(DocumentForm model)
    {
        var passport = await ImageSaver(model.Passport, model.UserId, "Passport");
        var nationalCard = await ImageSaver(model.NationalCard, model.UserId, "NationalCard");
        if (string.IsNullOrEmpty(passport) || string.IsNullOrEmpty(nationalCard))
        {
            return new ResultModel<bool>(false);
        }

        var res = await _customerRepository.CustomerDocuments(model, passport, nationalCard);
        return new ResultModel<bool>(res);
    }

    public async Task<ResultModel<bool>> SetAddress(AdressForm model)
        => new ResultModel<bool>(await _customerRepository.SetAdress(model));


    public async Task<string> ImageSaver(IFormFile model, long userId, string docType)
    {
        try
        {
            var file = model;
            var storePath = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build()
                .GetSection("storePath").Value;
            var folderName = Path.Combine("DocumentArchive", docType);
            var pathToSave = Path.Combine(storePath, folderName);
            if (file.Length > 0)
            {
                var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim();
                var a = fileName.Split(new[] { '.' });
                var extension = a.LastOrDefault().ToString();
                var fullPath = Path.Combine(pathToSave, userId + extension);

                if (!Directory.Exists(pathToSave))
                {
                    Directory.CreateDirectory(pathToSave);
                }

                using (var stream =
                       new FileStream(fullPath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite))
                {
                    await file.CopyToAsync(stream);
                }

                return fullPath;
            }
        }
        catch (Exception ex)
        {
            return null;
        }

        return null;
    }
}