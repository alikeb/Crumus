using CM.Models.BaseModels;
using CM.Models.Requests;

namespace CM.Service.IServices;

public interface ICustomerServices
{
    Task<ResultModel<bool>> CreateOne(CreateOne model);
    Task<ResultModel<bool>> CustomerDocuments(DocumentForm model);
    Task<ResultModel<bool>> SetAddress(AdressForm model);
}