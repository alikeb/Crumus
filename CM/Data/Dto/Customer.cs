using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

namespace CM.Data.Dto;

[Index(nameof(UserId), nameof(NationalCode), nameof(PassportNumber))]
public class Customer
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public long Id { get; set; }

    
    [ForeignKey("Users")] 
    public long UserId { get; set; }
    public string? NationalCode { get; set; }
    public string? PassportNumber { get; set; }
    public string? NationalCard { get; set; }
    public string? PassPort { get; set; }
    public long? CityId { get; set; }
    public string? ZipCode { get; set; }
    public string? Address { get; set; }
    public DateTime? CreatedAt { get; set; }
    public bool? IsRemoved { get; set; }
}