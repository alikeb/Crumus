using System.Diagnostics;
using System.Net;
using Newtonsoft.Json;
using RestSharp;

namespace CrumusGateway.Service;

public class BaseService
{

    private string baseUrl;
    private string OtpUrl;
    private string OBUrl;
    private string GeoUrl;
    private string TpUrl;
    private readonly IConfiguration _configuration;
   // private readonly ILogger _logger = Log.ForContext<BaseService>();


    public BaseService()
    {
        var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
        OtpUrl = config.GetSection("OtpUrl").Value;
        OBUrl = config.GetSection("OBUrl").Value;
        GeoUrl = config.GetSection("GeoUrl").Value;
        TpUrl = config.GetSection("TpUrl").Value;
    }
    public async Task<T?> HttpGetRequest<T>(string RequestAddress,int port)
    {
        try
        {
            baseUrl = PortChecker(port);

            var client = new RestClient(baseUrl);
            var request = new RestRequest(RequestAddress);

            var response = await  client.GetAsync(request);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                // _logger.Error(
                //     "\n-address: " + ApiBaseAddress + RequestAddress + "\n - respond: " + response.Content);
                throw new Exception();
            }

            var setting = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };


            return JsonConvert.DeserializeObject<T>(response.Content, setting);
        }
        catch (Exception e)
        {
            // var t = new ResultMo
            // {
            //     clientMessage = e.Message,
            //     code = 400,
            //     message = e.Message,
            //     statusCode = 400
            // };
            // _logger.Error("\n-address: " + ApiBaseAddress + RequestAddress + "\n - respond: " + e.Message);
            return JsonConvert.DeserializeObject<T>(null)!;
        }
    }
    public async Task<T?> HttpPostRequest<T>(string RequestAddress, string SerializedObject, int port)
    {
        try
        {
            baseUrl = PortChecker(port);

            var client = new RestClient(baseUrl);
            var request = new RestRequest(RequestAddress);
          //  request.AddHeader("Authorization", token != null ? token : "");
            request.AddHeader("Content-Type", "application/json");
            request.AddBody(SerializedObject, "application/json");
            var response = await client.ExecutePostAsync(request);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                // _logger.Error("\n-address: " + ApiBaseAddress + RequestAddress + "\n -input: " + SerializedObject +
                              // "\n - respond: " + response.Content);
            }

            if (string.IsNullOrEmpty(response.Content))
                throw new Exception(response.Content);

            return JsonConvert.DeserializeObject<T>(response.Content)!;
        }
        catch (Exception e)
        {
            return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(null))!;
        }
    }
    private string PortChecker(int portId)
        => portId switch
        {
            1 => OtpUrl,
            2=>OBUrl,
            3=>GeoUrl,
            4=>TpUrl,
            _ => OtpUrl
        };
}