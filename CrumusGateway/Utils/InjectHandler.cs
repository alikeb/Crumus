using CrumusGateway.Service;

namespace CrumusGateway.Utils;

public static class InjectHandler
{
    public static IServiceCollection InjectServices(this IServiceCollection services)
    {
        services.AddScoped<BaseService>();
       
        return services;
    }
}