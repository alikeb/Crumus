namespace CrumusGateway.Models.Otp.Input;

public class CheckOtp
{
    public string Email { get; set; }
    public string OtpId { get; set; }
    public string Code { get; set; }
}