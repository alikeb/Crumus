using CrumusGateway.Models.BaseModels;

namespace CrumusGateway.Models.Otp.Respond;

public class OtpModel:BaseResponseModel
{
    public SendOtp Model { get; set; }
}