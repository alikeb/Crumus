using CrumusGateway.Models.BaseModels;

namespace CrumusGateway.Models.Otp.Respond;

public class CheckOtpResponse:BaseResponseModel
{
    public bool Model { get; set; }
}