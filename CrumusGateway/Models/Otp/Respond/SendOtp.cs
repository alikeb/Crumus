using CrumusGateway.Models.BaseModels;

namespace CrumusGateway.Models.Otp.Respond;

public class SendOtp
{
    public string OtpId { get; set; }
    public string Code { get; set; }
}