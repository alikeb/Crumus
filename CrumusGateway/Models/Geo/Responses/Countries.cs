using CrumusGateway.Models.BaseModels;

namespace CrumusGateway.Models.Geo.Responses;

public class Countries:BaseResponseModel
{
    public List<Country> Model { get; set; }
}