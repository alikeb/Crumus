using CrumusGateway.Models.BaseModels;

namespace CrumusGateway.Models.Geo.Responses;

public class Country
{
    public int Id { get; set; }
    public string Code { get; set; }
    public string Symbol { get; set; }
    public string Name { get; set; }
    public string Flag { get; set; }
   // public object Provinces { get; set; }
}