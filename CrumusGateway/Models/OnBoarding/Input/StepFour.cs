namespace CrumusGateway.Models.OnBoarding.Input;

public class StepFour
{
    public long Id { get; set; }
    public string UserName { get; set; }
    public string Password { get; set; }
}