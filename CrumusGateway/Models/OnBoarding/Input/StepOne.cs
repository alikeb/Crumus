namespace CrumusGateway.Models.OnBoarding.Input;

public class StepOne
{
    public string Email { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public int Gender { get; set; }
    public DateTime BirthDate { get; set; }
}