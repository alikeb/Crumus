namespace CrumusGateway.Models.OnBoarding.Input;

public class StepTwo
{
    public IFormFile NationalCard { get; set; }
    public IFormFile Passport { get; set; }
    public long Id { get; set; }
    public string NationalCode { get; set; }
    public string PassportNumber { get; set; }
}