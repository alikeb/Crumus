namespace CrumusGateway.Models.OnBoarding.Input;

public class StepThree
{
    public long Id { get; set; }
    public string Address { get; set; }
    public string ZipCode { get; set; }
    public long CityId { get; set; }   
}