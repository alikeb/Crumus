using CrumusGateway.Models.BaseModels;

namespace CrumusGateway.Models.OnBoarding.Respond;

public class Initiate:BaseResponseModel
{
    public long Model { get; set; }
}