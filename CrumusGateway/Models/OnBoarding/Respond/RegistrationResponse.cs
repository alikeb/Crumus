using CrumusGateway.Models.BaseModels;

namespace CrumusGateway.Models.OnBoarding.Respond;

public class RegistrationResponse:BaseResponseModel
{
    public bool Model { get; set; }
}