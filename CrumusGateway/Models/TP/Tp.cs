using CrumusGateway.Models.BaseModels;

namespace CrumusGateway.Models.TP;

public class Tp
{
    public int CustomerId { get; set; }
    public int CustomerAgentId { get; set; }
    public int RecipientId { get; set; }
    public int RecipientAgentId { get; set; }
    public int SourceCurrencyId { get; set; }
    public int TargetCurrencyId { get; set; }
    public int SourceAmount { get; set; }
    public int TargetAmount { get; set; }
    public int RateId { get; set; }
    public int Fee { get; set; }
    public int Tax { get; set; }
    public int Status { get; set; }
    public string Description { get; set; }
}