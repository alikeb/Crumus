using CrumusGateway.Models.BaseModels;

namespace CrumusGateway.Models.TP;

public class AddTpRes:ResponseModel
{
    public  long Data { get; set; }
}