namespace CrumusGateway.Models.BaseModels;

public class BaseResponseModel
{
  
    public bool IsSuccess { get; set; }
    public string Message { get; set; }
    public int StatusCode { get; set; }
}