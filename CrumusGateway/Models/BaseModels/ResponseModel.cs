namespace CrumusGateway.Models.BaseModels;

public class ResponseModel
{
    public bool IsSuccess { get; set; }
    public int StatusCode { get; set; }
    public int HttpStatusCode { get; set; }
    public string Message { get; set; }
}