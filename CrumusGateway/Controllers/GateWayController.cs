using CrumusGateway.Models.BaseModels;
using CrumusGateway.Models.Geo.Responses;
using CrumusGateway.Models.OnBoarding.Input;
using CrumusGateway.Models.OnBoarding.Respond;
using CrumusGateway.Models.Otp.Respond;
using CrumusGateway.Service;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using CheckOtp = CrumusGateway.Models.Otp.Input.CheckOtp;


namespace CrumusGateway.Controllers;

[Produces("application/json")]
[Route("V1/[controller]")]
public class GateWayController : Controller
{
    private readonly BaseService _baseService;

    public GateWayController(BaseService baseService)
    {
        _baseService = baseService;
    }

    [HttpPost(nameof(SendOtp))]
    public async Task<IActionResult> SendOtp([FromBody] string Email)
    {
        var res = await _baseService.HttpPostRequest<OtpModel>("V1/Otp/SendOtp", JsonConvert.SerializeObject(Email), 1);
        return new Respond<SendOtp>().ActionRespond(new ResultModel<SendOtp>(new()
        {
            Code = res.Model.Code,
            OtpId = res.Model.OtpId
        }, res.IsSuccess, res.Message, res.StatusCode));
    }

    [HttpPost(nameof(CheckOtp))]
    public async Task<IActionResult> CheckOtp([FromBody] CheckOtp model)
    {
        var res = await _baseService.HttpPostRequest<CheckOtpResponse>("V1/Otp/CheckOtp",
            JsonConvert.SerializeObject(model), 1);
        return new Respond<bool>().ActionRespond(new ResultModel<bool>(res.Model));
    }

    [HttpPost(nameof(StepOne))]
    public async Task<IActionResult> StepOne([FromBody] StepOne model)
    {
        var res = await _baseService.HttpPostRequest<Initiate>("V1/OnBoarding/StepOne", JsonConvert.SerializeObject(model), 2);
        return new Respond<long>().ActionRespond(new ResultModel<long>(res.Model));
    }
    [HttpPost(nameof(StepTwo))]
    public async Task<IActionResult> StepTwo(StepTwo model)
    {
        var res = await _baseService.HttpPostRequest<bool>("V1/OnBoarding/StepTwo", JsonConvert.SerializeObject(model), 2);
        return new Respond<bool>().ActionRespond(new ResultModel<bool>(res));
    }
    [HttpPost(nameof(StepThree))]
    public async Task<IActionResult> StepThree([FromBody]StepThree model)
    {
        var res = await _baseService.HttpPostRequest<RegistrationResponse>("V1/OnBoarding/StepThree", JsonConvert.SerializeObject(model), 2);
        return new Respond<bool>().ActionRespond(new ResultModel<bool>(res.Model));
    }
    [HttpPost(nameof(StepFour))]
    public async Task<IActionResult> StepFour([FromBody]StepFour model)
    {
        var res = await _baseService.HttpPostRequest<RegistrationResponse>("V1/OnBoarding/StepFour", JsonConvert.SerializeObject(model), 2);
        return new Respond<bool>().ActionRespond(new ResultModel<bool>(res.Model));
    }
    
    [HttpGet(nameof(Countries))]
    public async Task<IActionResult> Countries()
    {
        var res = await _baseService.HttpGetRequest<Countries>("V1/Countries/GetAll", 3);
        return new Respond<Countries>().ActionRespond(new ResultModel<Countries>(res));
    }

}