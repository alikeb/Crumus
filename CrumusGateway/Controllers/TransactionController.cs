using CrumusGateway.Models.BaseModels;
using CrumusGateway.Models.Geo.Responses;
using CrumusGateway.Models.TP;
using CrumusGateway.Service;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CrumusGateway.Controllers;

public class TransactionController : Controller
{
    private readonly BaseService _baseService;

    public TransactionController(BaseService baseService)
    {
        _baseService = baseService;
    }
    [HttpPost(nameof(AddOne))]
    public async Task<IActionResult> AddOne([FromBody] Tp model)
    {
        
        var res = await _baseService.HttpPostRequest<AddTpRes>("Transaction/Add", JsonConvert.SerializeObject(model),4);
        return new Respond<long>().ActionRespond(new ResultModel<long>(res.Data));
    }
}